use axum::{
    extract::{Path, State},
    response::IntoResponse,
    routing::{get, put},
    Json, Router,
};

use serde_derive::{Deserialize, Serialize};
use sqlx::SqlitePool;
use std::{env, error::Error};
use tower_http::services::ServeDir;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenvy::dotenv()?;

    let db_path = env::var("DATABASE_URL")?;
    let pool = SqlitePool::connect(&db_path).await?;

    sqlx::migrate!().run(&pool).await?;

    let app = Router::new()
        .route("/todos", get(list_todos).post(create_todo))
        .route("/todos/:id", put(toggle_todo).delete(delete_todo))
        .nest_service("/", ServeDir::new("client/dist"))
        .with_state(pool);

    axum::Server::bind(&"0.0.0.0:8080".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();

    Ok(())
}

#[derive(Serialize)]
struct Todo {
    id: i64,
    task: String,
    done: bool,
}

async fn list_todos(State(db): State<SqlitePool>) -> impl IntoResponse {
    let todos = sqlx::query!(
        r#"
            SELECT id, task, done 
            FROM todos
            ORDER BY id
        "#
    )
    .fetch_all(&db)
    .await
    .unwrap();

    let mut response = Vec::new();

    for todo in todos {
        response.push(Todo {
            id: todo.id,
            task: todo.task,
            done: todo.done,
        });
    }

    Json(response)
}

#[derive(Deserialize)]
struct CreateTodo {
    task: String,
}

async fn create_todo(
    State(db): State<SqlitePool>,
    Json(todo): Json<CreateTodo>,
) -> impl IntoResponse {
    let mut conn = db.acquire().await.unwrap();

    let id = sqlx::query!(
        r#"
            INSERT INTO todos (task) 
            VALUES (?1)
        "#,
        todo.task
    )
    .execute(&mut *conn)
    .await
    .unwrap()
    .last_insert_rowid();

    Json(id)
}

async fn toggle_todo(State(db): State<SqlitePool>, Path(id): Path<i64>) -> impl IntoResponse {
    let rows_affected = sqlx::query!(
        r#"
            UPDATE todos 
            SET done = NOT done
            WHERE id = ?1
        "#,
        id
    )
    .execute(&db)
    .await
    .unwrap()
    .rows_affected();

    Json(rows_affected == 1)
}

async fn delete_todo(State(db): State<SqlitePool>, Path(id): Path<i64>) -> impl IntoResponse {
    let rows_affected = sqlx::query!(
        r#"
            DELETE FROM todos
            WHERE id = ?1
        "#,
        id
    )
    .execute(&db)
    .await
    .unwrap()
    .rows_affected();

    Json(rows_affected == 1)
}
